package my_data_types is
	type matrix is array (natural range <>, natural range <>) of bit;
end package my_data_types;
use work.my_data_types;
entity nmux is
	generic (i: integer := 4; j: integer := 2);
	port (b: in matrix (0 to i-1, j-1 downto 0);
	opcode: in integer range 0 to i-1;
	result: out bit_vector (j-1 downto 0));
end nmux;
architecture structure of nmux is
begin
	outgen: for k in j-1 downto 0 generate
		result(i) <= b(opcode, i);
	end generate outgen;	
end structure;